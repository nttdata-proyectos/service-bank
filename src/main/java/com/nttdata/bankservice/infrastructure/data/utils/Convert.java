package com.nttdata.bankservice.infrastructure.data.utils;

import com.nttdata.bankservice.domain.document.AccountDto;
import com.nttdata.bankservice.infrastructure.data.entity.Account;
import org.springframework.beans.BeanUtils;

public class Convert {
    public static AccountDto entityToDTO(Account account) {
        AccountDto accountDTO  = new AccountDto();
        BeanUtils.copyProperties(account, accountDTO);
        return accountDTO;

    }
    public static Account dtoToEntity(AccountDto accountDTO){
        Account account=new Account();
        BeanUtils.copyProperties(accountDTO,account);
        return account;
    }
}
