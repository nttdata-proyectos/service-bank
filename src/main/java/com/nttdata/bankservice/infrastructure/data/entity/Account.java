package com.nttdata.bankservice.infrastructure.data.entity;

import com.nttdata.bankservice.domain.document.AccountTypeDto;
import com.nttdata.bankservice.domain.document.ClientDto;
import com.nttdata.bankservice.domain.document.CreditCardDto;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Document(collection = "accounts")
public class Account {
    @Id
    private String id;
    private Double amount;
    private String clientDocumentNumber;
    private String accountNumber;
    private String cci;
    private Double maintenanceFee;  //tarifa de Mantenimiento
    private Integer maxLimitMonthlyMovements;
    private Integer dayAllowed;  //dia permitido
    private AccountTypeDto accountType;
    private ClientDto client;
    private CreditCardDto creditCard;
    private LocalDateTime createAt;
    private Boolean status;

}
