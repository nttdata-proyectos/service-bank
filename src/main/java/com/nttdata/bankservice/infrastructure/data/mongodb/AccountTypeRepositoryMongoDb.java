package com.nttdata.bankservice.infrastructure.data.mongodb;

import com.nttdata.bankservice.infrastructure.data.entity.AccountType;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface AccountTypeRepositoryMongoDb  extends ReactiveMongoRepository<AccountType,String> {
  Mono<AccountType> findByCode(String code);

}
