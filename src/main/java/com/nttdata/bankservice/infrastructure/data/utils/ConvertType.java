package com.nttdata.bankservice.infrastructure.data.utils;

import com.nttdata.bankservice.domain.document.AccountTypeDto;
import com.nttdata.bankservice.infrastructure.data.entity.AccountType;
import org.springframework.beans.BeanUtils;

public class ConvertType {
    public static AccountTypeDto entityToDTO(AccountType accountType) {
        AccountTypeDto accountDto  = new AccountTypeDto();
        BeanUtils.copyProperties(accountType, accountDto);
        return accountDto;

    }
    public static AccountType dtoToEntity(AccountTypeDto accountTypeDto){
        AccountType account=new AccountType();
        BeanUtils.copyProperties(accountTypeDto,account);
        return account;
    }
}
