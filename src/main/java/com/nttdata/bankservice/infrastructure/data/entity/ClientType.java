package com.nttdata.bankservice.infrastructure.data.entity;

public enum ClientType {

  PERSONAL, BUSINESS

}
