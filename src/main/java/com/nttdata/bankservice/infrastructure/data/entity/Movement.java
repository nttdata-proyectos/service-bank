package com.nttdata.bankservice.infrastructure.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Field;

public class Movement {
  @Field("created_by")
  @JsonIgnore
  private String createdBy;

  @CreatedDate
  @Field("created_date")
  @JsonIgnore
  private LocalDateTime createdDate = LocalDateTime.now();

  @Field("last_modified_by")
  @JsonIgnore
  private String lastModifiedBy;

  @LastModifiedDate
  @Field("last_modified_date")
  @JsonIgnore
  private LocalDateTime lastModifiedDate = LocalDateTime.now();
}
