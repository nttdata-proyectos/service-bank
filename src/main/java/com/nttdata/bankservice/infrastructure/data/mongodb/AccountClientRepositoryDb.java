package com.nttdata.bankservice.infrastructure.data.mongodb;

import com.nttdata.bankservice.infrastructure.data.entity.Account;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface AccountClientRepositoryDb extends ReactiveMongoRepository<Account,String> {
}
