package com.nttdata.bankservice.infrastructure.data.services.client.rest;


import com.nttdata.bankservice.domain.document.ClientDto;
import org.springframework.web.bind.annotation.GetMapping;
import reactor.core.publisher.Mono;


public interface ServiceClientRest {

    //@PostMapping("/register")
    //public Mono<ClientDTO> register(@RequestBody Mono<ClientDTO> clientDTO);

    @GetMapping("/list")
    public ClientDto findAll();
    //public Flux<ClientDTO> findAll();

    //@GetMapping("/delete/{id}")
    //public Mono<Void> delete(@PathVariable String id) ;

    @GetMapping("/list/{id}")
    public Mono<ClientDto> findById();
}
