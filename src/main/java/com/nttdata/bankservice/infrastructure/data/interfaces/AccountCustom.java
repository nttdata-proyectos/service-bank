package com.nttdata.bankservice.infrastructure.data.interfaces;

import com.nttdata.bankservice.domain.document.AccountDto;
import com.nttdata.bankservice.infrastructure.data.entity.Account;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface AccountCustom {
  Mono<AccountDto> findByAccountNumber(String accountNumber);

  Flux<AccountDto> findByClientIdAndAccountTypeId(String clientId, String accountTypeId);

  Flux<AccountDto> findByClientDocument(String documentNumber);

  Mono<Account> updateAccountAmount(Account account);
}
