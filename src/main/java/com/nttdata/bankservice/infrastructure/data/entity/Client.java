package com.nttdata.bankservice.infrastructure.data.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.ReadOnlyProperty;

@Data
@NoArgsConstructor
public class Client {

  private String id;
  @ReadOnlyProperty
  private String firstName;
  @ReadOnlyProperty
  private String surnames;
  private String documentNumber;
  private ClientType clientType;

}
