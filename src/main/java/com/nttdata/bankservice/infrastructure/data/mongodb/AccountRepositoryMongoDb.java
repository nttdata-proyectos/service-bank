package com.nttdata.bankservice.infrastructure.data.mongodb;

import com.nttdata.bankservice.domain.document.AccountDto;
import com.nttdata.bankservice.infrastructure.data.entity.Account;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface AccountRepositoryMongoDb extends ReactiveMongoRepository<Account,String>{

  //Mono<Account> findByTypeAndIdClients( String type, String  IdClient);
  //Flux<Account> findByIdClient(String  idClient);
Mono<AccountDto> findByAccountNumber(String accountNumber);
  //Flux<Account> findByCreateAtBerween(LocalDate startDate, LocalDate endDate);

//Mono<Account> findByClientDocument(String documentNumber);

  //Mono<Account> findByClientIdAndAccountTypeId(String clientId, String accountTypeId);


 default Mono<Account> updateAccountAmount(Account account, ReactiveMongoTemplate reactiveMongoTemplate) {
    Query query = new Query(Criteria.where("_id").is(new ObjectId(account.getId())));
    Update update = new Update().set("amount", account.getAmount());
    return reactiveMongoTemplate.findAndModify(query, update, Account.class).thenReturn(account);
  }

}
