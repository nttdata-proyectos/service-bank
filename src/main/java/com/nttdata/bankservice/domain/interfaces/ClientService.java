package com.nttdata.bankservice.domain.interfaces;

import com.nttdata.bankservice.domain.document.ClientDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * sdsd.
 */
public interface ClientService {
  public Flux<ClientDto> findAll();
  public Mono<ClientDto> findById(String id);

  public Mono<ClientDto> getClientByDocumentNumber(final String documentNumber);
}
