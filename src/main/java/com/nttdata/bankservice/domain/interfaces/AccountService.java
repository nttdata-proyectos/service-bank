package com.nttdata.bankservice.domain.interfaces;

import com.nttdata.bankservice.domain.document.AccountDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;



public interface AccountService {
    public Flux<AccountDto> findAll();
    public Mono<AccountDto> findById(String id);
    public Mono<AccountDto> findByAccountNumber(String accountNumber);
    public Mono<AccountDto> save(AccountDto accountDto);
    public Mono<Void> delete(String id);

    Mono<AccountDto> updateAccountAmount(String accountId, Double amount);

}
