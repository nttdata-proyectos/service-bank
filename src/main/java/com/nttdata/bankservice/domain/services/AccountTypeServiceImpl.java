package com.nttdata.bankservice.domain.services;

import com.nttdata.bankservice.domain.document.AccountTypeDto;
import com.nttdata.bankservice.domain.interfaces.AccountTypeService;
import com.nttdata.bankservice.infrastructure.data.mongodb.AccountTypeRepositoryMongoDb;
import com.nttdata.bankservice.infrastructure.data.utils.ConvertType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing AccountType.
 */
@Service
@RequiredArgsConstructor
public class AccountTypeServiceImpl implements AccountTypeService {
  private final AccountTypeRepositoryMongoDb typeRepository;
  @Override
  public Flux<AccountTypeDto> findAll() {
    return typeRepository.findAll()
        .map(ConvertType::entityToDTO);
  }
  @Override
  public Mono<AccountTypeDto> findById(String id) {
    return typeRepository.findById(id)
        .map(ConvertType::entityToDTO);
  }
  @Override
  public Mono<AccountTypeDto> findByCode(String code) {
    return typeRepository.findByCode(code)
        .map(ConvertType::entityToDTO);
  }
  @Override
  public Mono<AccountTypeDto> create(AccountTypeDto accountTypeDto) {
    return Mono.just(accountTypeDto)
        .map(ConvertType::dtoToEntity)
        .flatMap(typeRepository::insert)
        .map(ConvertType::entityToDTO);
  }
  @Override
  public Mono<AccountTypeDto> update(String id, AccountTypeDto accountTypeDto) {
    return typeRepository.findById(id)
        .flatMap(p -> Mono.just(accountTypeDto)
            .map(ConvertType::dtoToEntity)
            .doOnNext(e -> e.setId(id)))
        .flatMap(typeRepository::save)
        .map(ConvertType::entityToDTO);
  }

  /**
   * It deletes a type by its id
   *
   * @param id The id of the type to delete.
   * @return A Mono of type Void.
   */
  @Override
  public Mono<Void> delete(String id) {
    return typeRepository.deleteById(id);
  }
}
