package com.nttdata.bankservice.domain.services;

import static com.nttdata.bankservice.utils.MessageUtils.getMsg;

import com.nttdata.bankservice.domain.document.AccountDto;
import com.nttdata.bankservice.domain.document.enums.AccountType;
import com.nttdata.bankservice.domain.interfaces.AccountService;
import com.nttdata.bankservice.exception.AccountTypeNotFoundException;
import com.nttdata.bankservice.exception.BadRequestException;
import com.nttdata.bankservice.exception.ClientNotFoundException;
import com.nttdata.bankservice.infrastructure.data.mongodb.AccountRepositoryMongoDb;
import com.nttdata.bankservice.infrastructure.data.mongodb.AccountTypeRepositoryMongoDb;
import com.nttdata.bankservice.infrastructure.data.utils.Convert;
import com.nttdata.bankservice.infrastructure.data.utils.ConvertType;
import java.time.Duration;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.retry.Retry;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountServiceImp implements AccountService {
  //private static MessageSource messageSource;
  @Autowired
  private AccountRepositoryMongoDb accountRepository;
  @Autowired
  private AccountTypeRepositoryMongoDb typeRepository;

  @Autowired
  ClientServiceFeign ClientServiceWeb;

  private final ReactiveMongoTemplate reactiveMongoTemplate;

  @Override
  public Flux<AccountDto> findAll() {
    return accountRepository.findAll().map(Convert::entityToDTO);
  }

  @Override
  public Mono<AccountDto> findById(String id) {
    return accountRepository.findById(id).map(Convert::entityToDTO);
  }

  @Override
  public Mono<AccountDto> findByAccountNumber(String accountNumber) {
    return accountRepository.findByAccountNumber(accountNumber);
  }

  @Override
  public Mono<AccountDto> save(AccountDto accountDto) {
    return Mono.just(accountDto)
        .flatMap(this::existClient)
        .flatMap(this::existAccountNumber)
        .flatMap(this::existAccountType)
        .flatMap(this::validateMaxLimitMovement)
        .flatMap(this::validateDayAllowed)
        .map(Convert::dtoToEntity)
        .flatMap(this.accountRepository::save)
        .map(Convert::entityToDTO)
        .subscribeOn(Schedulers.boundedElastic());
  }
  /*public static String getMsg(String key) {

    return messageSource.getMessage(key, null, LocaleContextHolder.getLocale());
  }*/
  private Mono<AccountDto> existClient(AccountDto accountDto) {
    log.debug("Obtener Cliente por su numero de Document");
    return ClientServiceWeb.getClientByDocumentNumber(accountDto.getClientDocumentNumber())
        .switchIfEmpty(Mono.error(new ClientNotFoundException(
            getMsg("client.not.found"))))
        .doOnNext(accountDto::setClient)
        .retryWhen(Retry.fixedDelay(3, Duration.ofSeconds(1)))
        .thenReturn(accountDto);
  }
  private Mono<AccountDto> existAccountNumber(AccountDto accountDto) {
    return findByAccountNumber(accountDto.getAccountNumber())
        .flatMap(r -> Mono.error(new BadRequestException(getMsg("account.already"))))
        .thenReturn(accountDto);
  }
  private Mono<AccountDto> existAccountType(AccountDto accountDto) {
    return typeRepository.findByCode(accountDto.getAccountTypeCode())
        .switchIfEmpty(
            Mono.error(new AccountTypeNotFoundException(getMsg("account.type.not.found"))))
        .map(ConvertType::entityToDTO)
        .doOnNext(accountDto::setAccountType)
        .thenReturn(accountDto);
  }

  private Mono<AccountDto> validateMaxLimitMovement(AccountDto accountDto) {
    return Mono.just(accountDto)
        .<AccountDto>handle((dto, sink) -> {
          Integer maxLimit = dto.getMaxLimitMonthlyMovements();
          if (AccountType.SAVING.equalValue(dto.getAccountType().getCode()) && maxLimit <= 0) {
            sink.error(new BadRequestException(getMsg(
                "El número máximo de movimientos para el tipo de cuenta de Ahorros, debe ser mayor a cero")));
          } else if (AccountType.CURRENT.equalValue(dto.getAccountType().getCode())
              && maxLimit != 0) {
            sink.error(new BadRequestException(getMsg(
                "El número máximo de movimientos para el tipo de cuenta Corriente, no debe ser mayor a uno")));
          } else if (AccountType.FIXED_TERM.equalValue(dto.getAccountType().getCode())
              && maxLimit != 1) {
            sink.error(new BadRequestException(getMsg(
                "El tipo de cuenta Plazo Fijo no debe tener un número máximo de movimientos")));
          } else {
            sink.complete();
          }
        })
        .thenReturn(accountDto);
  }

  private Mono<AccountDto> validateDayAllowed(AccountDto accountDto) {
    return Mono.just(accountDto)
        .<AccountDto>handle((dto, sink) -> {
          Integer dayAllowed = dto.getDayAllowed();
          if (AccountType.SAVING.equalValue(dto.getAccountType().getCode())
              || AccountType.CURRENT.equalValue(dto.getAccountType().getCode())) {
            if (dayAllowed != 0) {
              sink.error(new BadRequestException(getMsg(
                  "No se debe especificar el tipo de cuenta Corriente el día del movimiento del mes")));
            }
          } else if (AccountType.FIXED_TERM.equalValue(dto.getAccountType().getCode())) {
            if (!(31 >= dayAllowed && dayAllowed > 0)) {
              sink.error(new BadRequestException(
                  getMsg(
                      "El tipo de cuenta de plazo fijo requiere que se especifique un día de movimiento válido en el mes")));
            }
          } else {
            sink.complete();
          }
        })
        .thenReturn(accountDto);
  }
  @Override
  public Mono<Void> delete(String id) {
    return accountRepository.deleteById(id);
  }
  @Override
  public Mono<AccountDto> updateAccountAmount(String accountId, Double amount) {
    return accountRepository.findById(accountId)
        .switchIfEmpty(Mono.error(new Exception(getMsg("account.not.found"))))
        .map(account -> {
          double totalAmount = Double.sum(amount, account.getAmount());
          account.setAmount(totalAmount);
          return account;
        })
        .flatMap(cuenta->accountRepository.updateAccountAmount(cuenta, reactiveMongoTemplate))
        .map(Convert::entityToDTO);
  }


}
