package com.nttdata.bankservice.domain.services;

import com.nttdata.bankservice.domain.document.ClientDto;
import com.nttdata.bankservice.domain.interfaces.ClientService;
import com.nttdata.bankservice.exception.ClientNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
@Slf4j
@Service
public class ClientServiceFeign implements ClientService  {

    private static final String STATUS_CODE = "Status code : {}";
    private final WebClient webClient;

    public ClientServiceFeign(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl("http://localhost:8885/client").build();
    }

    @Override
    public Flux<ClientDto> findAll() {
        return this.webClient.get().uri("/list")
                .retrieve()
                .bodyToFlux(ClientDto.class);
    }
    @Override
    public Mono<ClientDto> findById(String id) {
        return this.webClient.get().uri("/list/"+id)
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .onStatus(HttpStatus::is4xxClientError,
                error -> Mono.error(new RuntimeException("Verificar id del cliente")))
            .bodyToMono(ClientDto.class);
    }

    @Override
    public Mono<ClientDto> getClientByDocumentNumber(final String documentNumber) {
        return this.webClient.get()
            .uri("/document-number/{number}", documentNumber)
            .retrieve()
            .onStatus(HttpStatus::is4xxClientError, (clientResponse -> {
                log.info(STATUS_CODE, clientResponse.statusCode().value());
                if (clientResponse.statusCode().equals(HttpStatus.NOT_FOUND)) {
                    return Mono.error(new ClientNotFoundException(
                        "Cliente no disponible con Número de Documento : " + documentNumber,
                        clientResponse.statusCode().value()));
                }
                return clientResponse.bodyToMono(String.class)
                    .flatMap(response -> Mono
                        .error(
                            new ClientNotFoundException(response, clientResponse.statusCode().value())));
            }))
            .onStatus(HttpStatus::is5xxServerError, (clientResponse -> {
                log.info(STATUS_CODE, clientResponse.statusCode().value());
                return clientResponse.bodyToMono(String.class)
                    .flatMap(response -> Mono.error(new ClientNotFoundException(response)));
            }))
            .bodyToMono(ClientDto.class);
    }
}
