package com.nttdata.bankservice.domain.document;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountDto {
    private String id;
    @NotBlank(message = "clientDocumentNumber is required")
    @JsonProperty(access = WRITE_ONLY)
    private String clientDocumentNumber;
    @NotBlank(message = "accountNumber is required")
    private String accountNumber;
    @NotBlank(message = "accountTypeCode is required")
    @JsonProperty(access = WRITE_ONLY)

    private String accountTypeCode;
    @NotBlank(message = "cci is required")
    private String cci;
    @NotNull(message = "amount is required")
    @PositiveOrZero(message = "amount must be greater or equal to zero (0)")
    private Double amount;
    private Double maintenanceFee;  //tarifa de Mantenimiento
    private Integer maxLimitMonthlyMovements;
    private Integer dayAllowed;  //dia permitido
    private AccountTypeDto accountType;
    private ClientDto client;
    private CreditCardDto creditCard;
    private LocalDateTime createAt;
    private Boolean status;
}
