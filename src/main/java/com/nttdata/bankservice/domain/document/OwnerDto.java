package com.nttdata.bankservice.domain.document;

import lombok.Data;

@Data
public class OwnerDto {
  private String documentNumber;
  private String firstName;
  private String lastName;
}
