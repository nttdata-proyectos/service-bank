package com.nttdata.bankservice.domain.document;


import com.nttdata.bankservice.infrastructure.data.entity.ClientType;
import java.util.Date;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientDto {
    private String id;
    private String firstName;
    private String surnames;
    private String documentNumber;
    private String address;
    private String email;
    private ClientType clientType;
    private Date createAt;
    private Boolean status;
}
