package com.nttdata.bankservice.domain.document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {

  private String accountNumber;
  private String cci;
  private Double amount;
  private String productType;
  private ClientDto client;

}
