package com.nttdata.bankservice.domain.document.enums;

/*
CUENTA DE AHORROS
CUENTA CORRIENTE
CUENTA DE PLAZO FIJO
* */
public enum AccountType {


  SAVING("01"), CURRENT("02"), FIXED_TERM("03");

  private final String value;

  AccountType(String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return value;
  }

  public boolean equalValue(final String value) {
    return this.value.equalsIgnoreCase(value);
  }

}
