package com.nttdata.bankservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
public class BankServiceApplication   {

	public static void main(String[] args) {
		SpringApplication.run(BankServiceApplication.class, args);
	}

	/*@Override
	public void run(String... args) throws Exception {
		System.out.println("---------------------------");
		accountService.findByTypeAndIdClients("Cuenta Corriente","6345344").log()
				.map(AccountDTO::getType)
				.subscribe(System.out::println);
	}*/
}
