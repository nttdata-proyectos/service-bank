package com.nttdata.bankservice.aplication.rest;

import com.nttdata.bankservice.domain.document.AccountDto;
import com.nttdata.bankservice.domain.document.ClientDto;
import com.nttdata.bankservice.domain.interfaces.AccountService;
import com.nttdata.bankservice.domain.interfaces.ClientService;
import com.nttdata.bankservice.domain.services.ClientServiceFeign;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/account")
@Slf4j
public class AccountController {

  public static final String PLAZOFIJO = "Plazo Fijo";
  //private static final Logger log = LoggerFactory.getLogger(AccountController.class);
  @Autowired
  private AccountService accountService;
  @Autowired
  private ClientService clientServiceWebClient;
  @Autowired
  ClientServiceFeign ClientServiceWeb;

  @GetMapping
  public String index() {
    return "okAccount";
  }

  @GetMapping("/client-test")
  public Flux<ClientDto> clientRest() {
    return clientServiceWebClient.findAll();
  }

  @GetMapping("/{id}")
  public Mono<ResponseEntity<AccountDto>> findById(@PathVariable final String id) {
    log.info("find Account id: {}", id);
    return accountService.findById(id)
        .map(ResponseEntity::ok)
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }
  @GetMapping("/account-number/{accountNumber}")
  public Mono<ResponseEntity<AccountDto>> findByAccountNumber(@PathVariable String accountNumber) {
    log.info("find Account accountNumber: {}", accountNumber);
    return accountService.findByAccountNumber(accountNumber)
        .map(ResponseEntity::ok)
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }
  @PostMapping("/")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<AccountDto> create(@Valid @RequestBody AccountDto account) {
    return accountService.save(account);
  }
  @GetMapping("/client-number/{accountNumber}")
  public Mono<ResponseEntity<ClientDto>> findByClienNumber(@PathVariable String accountNumber) {
    log.info("find Account accountNumber: {}", accountNumber);
    return clientServiceWebClient.getClientByDocumentNumber(accountNumber)
        .map(ResponseEntity::ok)
        .defaultIfEmpty(ResponseEntity.notFound().build());
  }
  @GetMapping("list")
  public Flux<AccountDto> getAll() {
    log.info("List of Accounts");
    return accountService.findAll();
  }

  //MODIFICACIÓN DESDE CUENTA DE LA TRANSACCIÓN
  @PutMapping("/amount/{account-id}/{amount}")
  @ResponseStatus(HttpStatus.OK)
  public Mono<AccountDto> updateAmount(@PathVariable("account-id") String accountId,
                                       @PathVariable Double amount) {
    log.info("actualizar el monto de la cuenta {} por ID de cuenta: {} ", amount, accountId);
    return accountService.updateAccountAmount(accountId, amount);
  }


}
